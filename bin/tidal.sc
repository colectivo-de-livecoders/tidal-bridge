(
"»»» booting scsynth".postln;
s.options.numBuffers = 1024 * 128;
s.options.memSize = 8192 * 128;
s.options.numOutputBusChannels = 16;

s.reboot;

s.waitForBoot {
	"»»» starting dirt".postln;

	~dirt = SuperDirt(2, s);
    s.sync;

	~dirt.start(57120, [0, 2, 4, 6, 8, 10, 12, 14]);
	1.wait;

	~dirt.loadSoundFiles;
    3.wait;
	"»»» SuperDirt Loaded".postln;

	(0..7).do { |i|
		var left = i * 2 + 1;
		var right = left + 1;
		("jack_connect SuperCollider:out_" ++ left  + "system:playback_1").systemCmd;
		("jack_connect SuperCollider:out_" ++ right + "system:playback_2").systemCmd;
	};

	"»»» jack routed".postln;

	ServerMeter.new(s, 2, 16);


    if ('StageLimiter'.asClass.notNil) { StageLimiter.activate; };
	(
		SynthDef(\fms, {
			|out, amp=1, attack=0.001, sustain=1, accel, carPartial=1, modPartial=1, index=3, mul=0.25, detune=0.1, pan=0, freq|
			var env, mod, car, sig;
			//amp = amp * 0.9;
			env = EnvGen.ar(Env.perc(attack, 0.999, 1, -3), timeScale: sustain / 2, doneAction:2);
			mod = FSinOsc.ar(freq * modPartial * Line.kr(1,1+accel, sustain), 0, freq * index * LFNoise1.kr(5.reciprocal).abs);
			car = SinOsc.ar(([freq, freq+detune] * carPartial) + mod,    0, mul);
			sig = car * env;

			OffsetOut.ar(out, DirtPan.ar(sig * amp, ~dirt.numChannels, pan, env));
		}).add;


		SynthDef(\fatsaw, {
			| out=0, freq = 440, amp=0.1, pan = 0, gate=1, attack=0.01, decay=0.3, sustain=0.5, release=1, filterSpeed=100, filterFreqStart=300, filterFreqEnd=400, resonance=1, hfFilterFreq=1, hfFilterResonance=1 |
			var sig, env, filtFreq;
			env = EnvGen.ar(Env.adsr(attack, decay, sustain, release), gate, levelScale:0.5, doneAction:Done.freeSelf);
			sig = env*Splay.ar(LFSaw.ar(freq*[0.98,0.99,1.0,1.01,1.02],[LFNoise0.kr(2), LFNoise0.kr(1.9), LFNoise0.kr(2.1)]) + SinOsc.ar(freq*[0.98,0.99,1.0,1.01,1.02],[LFNoise0.kr(2), LFNoise0.kr(1.9), LFNoise0.kr(2.1)]));
			filtFreq = LFSaw.kr(filterSpeed,iphase:1).range(filterFreqStart,filterFreqEnd);
			sig = RLPF.ar(sig, filtFreq, resonance);
			sig = RHPF.ar(sig, hfFilterFreq, hfFilterResonance);
			2.do({
				sig = AllpassN.ar(sig, 0.050, [0.050.rand, 0.050.rand], 1);
			});

			OffsetOut.ar(out, DirtPan.ar(amp*sig.tanh, ~dirt.numChannels, pan));
		}).add;


		SynthDef(\rhod, {
			| // standard meanings
			out = 0, freq = 440, gate = 1, pan = 0, amp = 0.1,
			// all of these range from 0 to 1
			vel = 0.8, modIndex = 0.2, mix = 0.2, lfoSpeed = 0.4, lfoDepth = 0.1
			|
			var env1, env2, env3, env4;
			var osc1, osc2, osc3, osc4, snd;

			lfoSpeed = lfoSpeed * 12;

			freq = freq * 2;

			env1 = EnvGen.ar(Env.adsr(0.001, 1.25, 0.0, 0.04, curve: \lin));
			env2 = EnvGen.ar(Env.adsr(0.001, 1.00, 0.0, 0.04, curve: \lin));
			env3 = EnvGen.ar(Env.adsr(0.001, 1.50, 0.0, 0.04, curve: \lin));
			env4 = EnvGen.ar(Env.adsr(0.001, 1.50, 0.0, 0.04, curve: \lin));

			osc4 = SinOsc.ar(freq * 0.5) * 2pi * 2 * 0.535887 * modIndex * env4 * vel;
			osc3 = SinOsc.ar(freq, osc4) * env3 * vel;
			osc2 = SinOsc.ar(freq * 15) * 2pi * 0.108819 * env2 * vel;
			osc1 = SinOsc.ar(freq, osc2) * env1 * vel;
			snd = Mix((osc3 * (1 - mix)) + (osc1 * mix));
			snd = snd * (SinOsc.ar(lfoSpeed) * lfoDepth + 1);

			// using the doneAction: 2 on the other envs can create clicks (bc of the linear curve maybe?)
			snd = snd * EnvGen.ar(Env.asr(0, 1, 0.1), gate, doneAction: 2);
			snd = Pan2.ar(snd, pan, amp);
			
			OffsetOut.ar(out, DirtPan.ar(snd, ~dirt.numChannels, pan));
		}).add;
		
		
		SynthDef(\rhodes, {|out, sustain=1, pan, accelerate, freq |
			var sig, in, n = 6, max = 0.04, min = 0.01, delay, pitch, detune, hammer, amp, env;
			amp = 0.9;
			freq = freq.cpsmidi;
			hammer = Decay2.ar(Impulse.ar(0.001), 0.008, 0.04, LFNoise2.ar([2000,4000].asSpec.map(amp), 0.25));
			sig = Mix.ar(Array.fill(3, { arg i;
				detune = #[-0.04, 0, 0.03].at(i);
				delay = (1/(freq + detune).midicps);
				CombL.ar(hammer, delay, delay, 50 * amp)
			}) );

			sig = HPF.ar(sig,50);
			env = EnvGen.ar(Env.perc(0.0001,sustain, amp * 4, -1), doneAction:2);
			OffsetOut.ar(out, DirtPan.ar(sig, ~dirt.numChannels, pan, env));
		}).add;


		SynthDef(\bass8, {
			arg out = 0, amp = 0.1, gate = 1, pan = 0, freq = 200;
			var sig;
			var osc = Array.newClear(6);
			var env = Array.newClear(6);

			env[0] = EnvGen.kr(Env([0,1,0.051,0],[0.001,0.01,0.8], [4,-8]), 1);
			env[1] = EnvGen.kr(Env([0,1,0.051,0],[0.005,0.5,1.5], [0,-8], releaseNode:2), 1);
			env[2] = EnvGen.kr(Env([0,1,1,0],[0.01,0.01,0.2], [0,0,-4], releaseNode:2), gate);
			env[3] = EnvGen.kr(Env([0,1,0],[0.002,2.8], [0,-4]), 1);
			env[4] = EnvGen.kr(Env([0,1,1,0],[0.001,0.1,0.8], [4,0,-4], releaseNode:2), gate);
			env[5] = EnvGen.kr(Env([0,1,0],[0.001,3.0], [0,-4]), 1);

			freq = (freq / 4) * ((0..1)/1 - 0.5 * 0.0007 + 1);

			osc[0] = SinOsc.ar(freq * 11 + 0) * env[0];
			osc[1] = SinOsc.ar(freq * 6 * ( osc[0] * 2.5 + 1 )) * env[1];
			osc[2] = SinOsc.ar(freq * 2 * 1 + 0) * env[2];
			osc[3] = SinOsc.ar(freq * 1 * ( osc[2] * 2.5 + 1 ) + 0) * env[3];
			osc[4] = SinOsc.ar(freq * 1 * ( osc[1] * 2.5 + 1 ) * (osc[3] * 2.5 + 1)) * env[4];
			osc[5] = SinOsc.ar(freq * 2) * env[5];

			sig = osc * DC.ar([0.0, 0.0, 0.0,  0.0, 0.5, 0.5]);
			sig = sig * 0.5;
			sig = sig.flop.sum;
			sig = sig * EnvGen.ar(\adsr.kr( Env.adsr(0.001,0,1,0.01, 1,-1) ), gate, doneAction:2);
			sig = sig * AmpComp.kr(freq);
			sig = sig.sum;
			Out.ar(out, Pan2.ar(sig, pan, amp));

		}).add;


		SynthDef(\cs80lead, {

			arg freq=880, amp=0.5, attack=0.75, decay=0.5, sustain=0.8, release=1.0, fatt=0.75, fdecay=0.5, fsus=0.8, frel=1.0,
			cutoff=200, pan=0, dtune=0.002, vibrate=4, vibdepth=0.015, gate=1.0, ratio=1,out=0,cbus=1;
			var env,fenv,vib,ffreq,sig;
			cutoff=In.kr(cbus);
			env=EnvGen.kr(Env.adsr(attack,decay,sustain,release),gate,levelScale:1,doneAction:2);
			fenv=EnvGen.kr(Env.adsr(fatt,fdecay,fsus,frel,curve:2),gate,levelScale:1,doneAction:2);
			vib=SinOsc.kr(vibrate).range(-1*vibdepth,vibdepth)+1;
			freq=Line.kr(freq,freq*ratio,5);
			freq=freq*vib;
			sig=Mix.ar(Saw.ar([freq,freq*(1+dtune)]));
			// keep this below nyquist!!
			ffreq=max(fenv*freq*12,cutoff)+100;
			sig=LPF.ar(sig,ffreq);


			OffsetOut.ar(out, DirtPan.ar(sig, ~dirt.numChannels, pan, env));


		}).add;	


		SynthDef(\acid309, {|out, sustain=1, pan, accelerate, freq |
			var env1, env2, son, pitch, amp, gate;
			gate=1;
			amp = 0.5;
			pitch = freq.cpsmidi;
			pitch = Lag.kr(pitch, 0.12 * (1-Trig.kr(gate, 0.001)) * gate);
			env1 = EnvGen.ar(Env.new([0, 1.0, 0, 0], [0.001, 2.0, 0.04], [0, -4, -4], 2), gate, amp, doneAction:2);
			env2 = EnvGen.ar(Env.adsr(0.001, 0.8, 0, 0.8, 70, -4), gate);
			son = LFPulse.ar(pitch.midicps, 0.0, 0.51, 2, -1);

			son = RLPF.ar(son, (pitch + env2).midicps, 0.3);

			OffsetOut.ar(out, DirtPan.ar(son, ~dirt.numChannels, pan, env1));
		}).add;		
	)
}
)